<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Ruta para listas los usuarios registrados
Route::get('/', 'UserController@index');

// Ruta para guardar los futuros usuarios
Route::post('users', 'UserController@guardar')->name('users.guardar');

// Ruta para eliminar los usuarios
Route::delete('users/{users}', 'UserController@destroy')->name('users.destroy');