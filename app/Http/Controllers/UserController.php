<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function index() {
        //Variable para almacenar los usuarios de la BD
        $users = User::latest()->get();

        //Retornar los usuarios por medio de la vista index
        return view('users.index', [
            //Se manda como arreglo
            'users' => $users
        ]);
    }

    public function guardar(Request $request) {

        User::create([
            'name' => $request-> name,
            'email' => $request-> email,
            'password' => bcrypt($request-> password)
        ]);

        return back();

    }

    public function destroy(User $user){
        $user->delete();
        return back();
    }

}
